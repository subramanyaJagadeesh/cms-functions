'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mongodb = require('mongodb');

var _mongodb2 = _interopRequireDefault(_mongodb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var videoRouter = _express2.default.Router();


var MongoClient = _mongodb2.default.MongoClient;

var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true";

videoRouter
.post('/:page',function(req,res){
    var page = req.params.page;
    var content = req.body.content;
    MongoClient.connect(uri, function (err, client) {
        if (err) res.send('error:' + err);
        var db = client.db("cms");
        db.collection(page).update({type:'videos'},{$set:{content:content}}, function(err, result){
            if (err) res.send('error'+err);
            else
                res.send('successful');
        })
        client.close();
    })
})

.get('/:page',function(req,res){
    var page = req.params.page;
    MongoClient.connect(uri, function (err, client) {
        if (err) res.send('error:' + err);
        var db = client.db("cms");
        db.collection(page).aggregate([
            {$match:{type:'videos'}},
            {$project:{content:1,_id:0}}
        ]).toArray(function(err, result){
            if (err) res.send('error'+err);
            else
                res.send(result[0].content);
        })
        client.close();
    })
})

exports.default = videoRouter;
