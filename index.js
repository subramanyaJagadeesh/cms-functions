// import functions from 'firebase-functions';
// import express from 'express';
// const app = express();
// import bodyParser from 'body-parser';
// import busboy from 'connect-busboy';
// import {} from 'dotenv/config'
// app.use(busboy());
// app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));

// import authRouter from './auth/authRouter';
// import postRouter from './post/postRouter';
// import bucketRouter from './buckets/bucketRouter';
// import imageRouter from './image/imageRouter';
// app.use('/api/auth', authRouter);
// app.use('/api/post', postRouter);
// app.use('/api/buckets', bucketRouter);
// app.use('/api/upload', imageRouter);

// // // Create and Deploy Your First Cloud Functions
// // // https://firebase.google.com/docs/functions/write-firebase-functions
// //
// export default app = functions.https.onRequest(app);

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var cors = require("cors");

var _firebaseFunctions = require('firebase-functions');

var _firebaseFunctions2 = _interopRequireDefault(_firebaseFunctions);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var busboy = require('connect-busboy');

// var _connectBusboy2 = _interopRequireDefault(_connectBusboy);

require('dotenv/config');

var _authRouter = require('./auth/authRouter');

var _authRouter2 = _interopRequireDefault(_authRouter);

var _postRouter = require('./post/postRouter');

var _postRouter2 = _interopRequireDefault(_postRouter);

var _bucketRouter = require('./buckets/bucketRouter');

var _bucketRouter2 = _interopRequireDefault(_bucketRouter);

var _imageRouter = require('./image/imageRouter');

var _imageRouter2 = _interopRequireDefault(_imageRouter);

var _videoRouter = require('./video/videoRouter');

var _videoRouter2 = _interopRequireDefault(_videoRouter);

var _customRouter = require('./custom/customRouter');

var _customRouter2 = _interopRequireDefault(_customRouter);


function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var whitelist = ['https://postgame-cdn.firebaseapp.com','http://localhost:3000','https://beta.postgame.in','https://www.beta.postgame.in','https://postgame.in','https://www.postgame.in'];
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}
var app = (0, _express2.default)();
app.use(cors({origin:'*'}));
app.use(busboy());
app.use(_bodyParser2.default.json({ limit: '50mb' }));
app.use(_bodyParser2.default.urlencoded({ limit: '50mb', extended: true }));


app.use(function(req, res, next) {
  if (req.method === `OPTIONS`) {
    res.set('Access-Control-Allow-Origin', 'https://postgame-cdn.firebaseapp.com,http://localhost:3000,http://localhost:8080,https://beta.postgame.in,https://www.beta.postgame.in,https://postgame.in,https://www.postgame.in')
       .set('Access-Control-Allow-Methods', 'GET, POST')
       .status(200);
       return;
  }
  // else{
  //   console.log(req)
  // }
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
// var Jimp = require('jimp');
// var cloudinary = require('cloudinary');
// cloudinary.config({ 
//   cloud_name: 'postgame', 
//   api_key: '523833215654296', 
//   api_secret: 'qlntIjAZK7iqEpamwdHVOdyGSKc' 
// });
// var mongo = require('mongodb');
// var mongoClient = mongo.MongoClient;
// var resultArr = [];
// var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true";
// // function getImages(_callback){
// // // //   var tempResult = [];
//   mongoClient.connect(uri, function(err, client) {
//     if (err) res.send('error:'+err);
//     const db = client.db("cms");
//     db.collection('world_cup').find({}).toArray(function(err,result){
//       resultArr = result;
//       getImages(resultArr);
//     })
    
//       client.close();
//   });
// // }
// // }
// function getImages(resultArr){
//     resultArr.forEach((el)=>{
//       mongoClient.connect(uri, function(err, client) {
//         if (err) res.send('error:'+err);
//         const db = client.db("cms");
//         db.collection('world_cup').updateOne({_id:el._id},{$set:{pollID:shortid.generate()}},function(err,result){
//           if(err) throw err;
//           else{
//             console.log('successful');
//           }
//         }) 
//         client.close(); 
//       })
            
//   });
// }

app.listen(8000,function(){
  console.log('listening')
})

app.use('/api/auth', _authRouter2.default);
app.use('/api/post', _postRouter2.default);
app.use('/api/buckets', _bucketRouter2.default);
app.use('/api/upload', _imageRouter2.default);
app.use('/api/videos',_videoRouter2.default);
app.use('/api/custom',_customRouter2.default);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.default = _firebaseFunctions2.https.onRequest(app);
