'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mongodb = require('mongodb');

var _mongodb2 = _interopRequireDefault(_mongodb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var customRouter = _express2.default.Router();

var MongoClient = _mongodb2.default.MongoClient;

var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true";

var moment = require("moment");

customRouter

//to get today's match
.get("/todays_match/:page",function(req,res){
    var page = req.params.page;
    MongoClient.connect(uri, function (err, client) {
        if (err) res.send('error:' + err);
        var db = client.db("cms");
        db.collection(page).find({date:moment().format("ddd DD MMMM")}).toArray(function(err,result){
           result.forEach(el=>{
                if(result.length==1)
                    res.send(el);
                else if(el.IST>moment().format("HHmm"))
                    res.send(el);
           })
        })
        client.close();
    })
})

//to get points table
.get("/points_table/:page",function(req,res){
    var page = req.params.page;
    MongoClient.connect(uri, function (err, client) {
        if (err) res.send('error:' + err);
        var db = client.db("cms");
        db.collection(page).find({type:"points_table"}).toArray(function(err,result){
           res.send(result[0].content); 
        })
        client.close();
    })
})

//to get today's poll
.post("/poll/:page",function(req,res){
    var page = req.params.page;
    var key = req.body.key;
    var id  = req.body.id;
    MongoClient.connect(uri, function (err, client) {
        if (err) res.send('error:' + err);
        var db = client.db("cms");
        if(key==0){
            db.collection(page).update({$and:[{date:moment().format("ddd DD MMMM")},{pollID:id}]},{$inc:{team1Count:1}},function(err,result){
            })
            db.collection(page).find({$and:[{date:moment().format("ddd DD MMMM")},{pollID:id}]}).toArray(function(err,resul){
                res.send(resul[0])
            })
        }
        else{
            db.collection(page).update({$and:[{date:moment().format("ddd DD MMMM")},{pollID:id}]},{$inc:{team2Count:1}},function(err,result){          
            })
            db.collection(page).find({$and:[{date:moment().format("ddd DD MMMM")},{pollID:id}]}).toArray(function(err,resul){
                res.send(resul[0])
            })
        }
        client.close();
    })
})

exports.default = customRouter;
