// import express from 'express';
// const postRouter = express.Router();
// import Mongo from 'mongodb';

// var MongoClient = Mongo.MongoClient;

// var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true";

// postRouter
//   //to post a new article
//   .post('/articles', (req, res) => {
//       var postObj = req.body;
//       var sport = req.body.sport;
//       MongoClient.connect(uri, function(err, client) {
//         if (err) res.send('error:'+err);
//         const db = client.db("cms");
//         db.collection('articles').insertOne(postObj, (err, result) => {
//           if (err) throw err;
//           else
//             res.send('www.postgame.in/'+sport+'/'+postObj.name);
//         })
//         client.close();
//       });
//   })
//   //to get all the articles in archive
//   .get('/articles_archive/:from',(req,res) => {
//     var from  = parseInt(req.params.from);
//     MongoClient.connect(uri, function(err, client) {
//       if(err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection('articles').find({}).limit(30).skip(from).toArray((err,result)=>{
//         if(err) throw err;
//         else{
//           res.send(result);
//         }
//       });
//       client.close();
//     })
//   })
//   //to get a specific article
//   .get('/articles/:id',(req,res)=>{
//     var id = req.params.id;
//     var str = "ObjectId"
//     MongoClient.connect(uri, function(err, client) {
//       if(err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection('articles').find({id:id}).toArray((err,result)=>{
//         if(err) throw err;
//         else{
//               res.setHeader('Content-Type', 'application/json');
//               res.json(result);
//             }
//       })
//       client.close();
//     })
//   })
//   //to get suggested articles
//   .post('/articles/suggested',(req,res)=>{
//     var type = req.body.type;
//     var sport  = req.body.sport;
//     if(type == 'featured'){
//       MongoClient.connect(uri, function(err, client) {
//         if(err) res.send('error:'+err);
//         const db = client.db("cms");
//         db.collection('articles').find({wasFeatured:true},(err,result)=>{
//           if(err) res.send('error');
//           else{
//             res.setHeader('Content-Type', 'application/json');
//             res.send(result);
//           }
//         }).limit(4);
//         client.close();
//       })
//     }
//     else{
//       MongoClient.connect(uri, function(err, client) {
//         if(err) res.send('error:'+err);
//         const db = client.db("cms");
//         db.collection('articles').find({sport:sport},(err,result)=>{
//           if(err) res.send('error');
//           else{
//             res.setHeader('Content-Type', 'application/json');
//             res.send(result);
//           }
//         }).limit(4);
//         client.close();
//       })
//     }
//   })
// export default postRouter;


'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mongodb = require('mongodb');

var _mongodb2 = _interopRequireDefault(_mongodb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var postRouter = _express2.default.Router();


var MongoClient = _mongodb2.default.MongoClient;

var striptags = require('striptags');

var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true";

postRouter
//to post a new article
.post('/articles', function (req, res) {
  var postObj = req.body;
  var sport = req.body.sport;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection('articles').insertOne(postObj, function (err, result) {
      if (err) throw err;else res.send('www.postgame.in/' + sport + '/' + postObj.headline);
    });
    client.close();
  });
})

//to fetch articles for feed
.get('/articles',function(rew,res){
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection('articles').aggregate([
      {$project:{key:1,headline:1,coverImg:1,sport:1,content:1,authorData:1,publishedOn:1,coverImg:1}}
    ]).toArray(function (err, result) {
      if (err) throw err;else {
        result.forEach(element => {
          element.content = striptags(element.content);
        })
        res.send(result);
      }
    });
    client.close();
  });
})
// to get article key
.post('/articles_key',function(req,res){
  var id = req.body.id;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection('articles').find({id:id}).toArray(function (err, result) {
      if (err) throw err;else {
        res.send({key:result[0].key});
      }
    });
    client.close();
  });
})

//To get the article count to display in the dashboard
// .post('/article_count', function (req, res) {
//   var from = req.body.from;
//   var to  = req.body.to;
//     MongoClient.connect(uri, function (err, client) {
//       if (err) res.send('error:' + err);
//       var db = client.db("cms");
//       // db.collection('articles').find().toArray(function (err, result) {
//       //   if (err) res.send('error'+err);else {
//       //     res.forEach( function (curr) {
//       //       db.collection('articles').update( {_id : curr._id},
//       //         {$set : {newPub : curr.publishedOn}})
//       //     })
//       //     res.send(result);
//       //   }
//       // })
//       db.collection('articles').find().forEach( function (curr) {
//         print( "ID: " + curr._id );
//         db.collection('articles').update( {_id : curr._id},
//           {$addToSet : {myArray : curr._id}})
//           res.send('SUCCESS');
//       })
//       client.close();
//     });
//   // }
// })

//to get all the articles in archive
.get('/articles_archive/:from', function (req, res) {
  var from = parseInt(req.params.from);
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection('articles').aggregate([
      {$project:{id:1,headline:1,coverImg:1,sport:1}}
    ]).toArray(function (err, result) {
      if (err) throw err;else {
        res.send(result);
      }
    });
    client.close();
  });
})

//to get a specific article
.get('/articles/:key', function (req, res) {
  var key = req.params.key;
  // var str = "ObjectId";
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection('articles').find({ key: key }).toArray(function (err, result) {
      if (err) throw err;else {
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
      }
    });
    client.close();
  });
})
//to get suggested articles
.post('/articles/suggested', function (req, res) {
  // var type = req.body.type;
  var sport = req.body.sport;
  var key  = req.body.key;
  // if (type == 'featured') {
  //   MongoClient.connect(uri, function (err, client) {
  //     if (err) res.send('error:' + err);
  //     var db = client.db("cms");
  //     db.collection('articles').find({ wasFeatured: true }).limit(4).toArray(function (err, result) {
  //       if (err) res.send('error');else {
  //         res.setHeader('Content-Type', 'application/json');
  //         res.send(result);
  //       }
  //     })
  //     client.close();
  //   });
  // } else {
    MongoClient.connect(uri, function (err, client) {
      if (err) res.send('error:' + err);
      var db = client.db("cms");
      db.collection('articles').find({$and:[{sport: sport},{key:{'$ne':key}}]}).project({key:1,id:1,headline:1,coverImg:1,sport:1}).limit(4).sort({$natural:-1}).toArray(function (err, result) {
        if (err) res.send('error'+err);else {
          res.setHeader('Content-Type', 'application/json');
          res.send(result);
        }
      })
      client.close();
    });
  // }
})
//to add external articles
.post('/external_article',function(req,res){
  var postObj = req.body;
  var sport = req.body.sport;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(sport).update({title:'curated'},{$push:{content:postObj}}, function (err, result) {
      if (err) throw err;
      else res.send('successful');
    });
    client.close();
  });
});
exports.default = postRouter;