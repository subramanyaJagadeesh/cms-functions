// import express from 'express';
// const bucketRouter = express.Router();
// import Mongo from 'mongodb';

// var MongoClient = Mongo.MongoClient;

// var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true";

// bucketRouter
// // to hero bucket
//   .get('/:page/hero', (req, res) => {
//       var page = req.params.page;
//       var resultArr = [] ;
//       MongoClient.connect(uri, function(err, client) {
//         if (err) res.send('error');
//         const db = client.db("cms");
//         db.collection(page).find({type:'hero'},(err, result) => {
//           if (err) res.send('error');
//           else{
//             result.forEach((ele)=>{
//               if(ele.type=='hero')
//                 resultArr.push(ele);
//             }).then(()=>{
//               res.send(resultArr);
//             })
//           } 
//         });
        
//         client.close();
//       });
//   })
//   //to get bucket ids
//   .get('/:page/:title',(req,res)=>{
//     var page = req.params.page;
//     var title = req.params.title;
//     var content =[];
//     MongoClient.connect(uri, function(err, client) {
//       if (err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection(page).find({title:title}).toArray((err,result)=>{
//         if(err) console.log(err);
//         content = result[0].content;
//         res.send(content);
//       });
//       client.close();
//     })
//   })
//   //to get articles in featured and carousel buckets
//   .post('/get_articles',(req,res)=>{
//     var content = [];
//     content = req.body.content;
//     var resultArr = [];
//     MongoClient.connect(uri, function(err, client) {
//       if (err) res.send('error:'+err);
//       const db = client.db("cms");
//       for(var i=0;i<content.length;i++){
//         db.collection('articles').find({id:content[i]}).toArray((err,result)=>{
//             resultArr.push(result[0]);
//             if(i==content.length)
//               res.send(resultArr);
//         })
//       }
//       client.close();
//     })
//   })
//   //to get videos and tweets
//   .get('/:page/videos_tweets', (req, res) => {
//     var page = req.params.page;
//     MongoClient.connect(uri, function(err, client) {
//       if (err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection(page).find({$or:[{type:'video'},{type:'tweets'}]}).toArray((err, result) => {
//         if (err) res.send('error');
//           else{
//             res.send(result)
//           } 
//       })
//       client.close();
//     });
//   })
//   //to fetch freelist
//   .post('/fetch_freelist',(req,res)=>{
//     var content = req.body.content;
//     var from  = req.body.cursor;
//     var resultArr = {};
//     MongoClient.connect(uri, function(err, client) {
//       if (err) res.send('error:'+err);
//       const db = client.db("cms");
//       for(var i = from; i>from+5 ;i++){
//         el  = content[i];
//         db.collection('articles').find({id:el}).toArray((err,result)=>{
//             resultArr.push(result[0]);
//         })
//       }
//       client.close();
//     })
//     res.send(resultArr);
//   })
//   // to add a new bucket
//   .post('/:page/add_new_bucket',(req,res) => {
//     var page = req.params.page;
//     var title = req.body.title;
//     var type = req.body.type;
//     var limit = req.body.limit;
//     MongoClient.connect(uri, function(err, client) {
//       if(err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection(page).insertOne({title:title,type:type,content:[],limit:limit,size:0},(err,result)=>{
//         if(err) res.send('error');
//         else{
//           res.send('succcessfull');
//         } 
//       })
//       client.close();
//     })
//   })
//   //to fetch bucket names
//   .get('/:page/bucket_names',(req,res)=>{
//     var page = req.params.page;
//     var pageBuckets = [];
//     var obj = {};
//     MongoClient.connect(uri, (err, client)=> {
//       if(err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection(page).find({$or:[{type:'single'},{type:'carousel'},{type:'freelist'}]},(err,result)=>{
//         if(err) res.send('error');
//         else{
//           result.forEach(el=>{
//             pageBuckets.push(el);
//               console.log('1'+pageBuckets) //1
//           }).then(()=>{
//             res.send(pageBuckets);
//           })
//         }
//       })
//       client.close();
//     })
//   })

//   // to update a bucket with an article
//   .post('/update_bucket',(req,res)=>{
//     var id = req.body.id; 
//     var page = req.body.sport;
//     var newBucket = req.body.currentBucket;
//     var content = [];
//     MongoClient.connect(uri, function(err, client) {
//       if(err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection(page).find({title:newBucket}).toArray((err,result)=>{
//         console.log(result[0].size+" "+result[0].limit);
//         if(err) throw res.send('1st error');
//         else if(result[0].size == result[0].limit && result[0].type!='freelist'){
//           res.send(page+"-"+newBucket+' is Full')
//         }
//       })
//       db.collection(page).update({content:id},{$pull:{content:id},$inc:{size:-1}},(err,result)=>{
//         if(err) res.send('3rd error');
//       })
//         db.collection(page).update({title:newBucket},{$push:{content:id},$inc:{size:1}},(err,result)=>{
//           if(err) res.send('4th error');
//           else{
//             res.send('successfull')
//           } 
//         })
//       client.close();
//     })
//   })
//   // to change the name of bucket
//   .put('/:page/change_name',(req,res)=>{
//     var page = req.params.page;
//     var title = req.body.title;
//     var type = req.body.type;
//     MongoClient.connect(uri, function(err, client) {
//       if(err) res.send('error:'+err);
//       const db = client.db("cms");
//       db.collection(page).updateOne({type:type},{$set:{title:title}},(err,result)=>{
//         if(err) throw err;
//         else{
//           res.send('succcessfull');
//         } 
//       })
//       client.close();
//     })
//   })
// export default bucketRouter;

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mongodb = require('mongodb');

var _mongodb2 = _interopRequireDefault(_mongodb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bucketRouter = _express2.default.Router();


var MongoClient = _mongodb2.default.MongoClient;

var uri = "mongodb://superUser:cartel_5@carteldemo-shard-00-00-t6wq6.mongodb.net:27017,carteldemo-shard-00-01-t6wq6.mongodb.net:27017,carteldemo-shard-00-02-t6wq6.mongodb.net:27017/test?ssl=true&replicaSet=CartelDemo-shard-0&authSource=admin&retryWrites=true";

bucketRouter
// to hero bucket
// .get('/:page/hero', function (req, res) {
//   var page = req.params.page;
//   var resultArr = [];
//   MongoClient.connect(uri, function (err, client) {
//     if (err) res.send('error');
//     var db = client.db("cms");
//     db.collection(page).find({ type: 'hero' }).toArray(function(err, result) {
//       if (err) res.send('error');
//       else {
//         res.send(result[0]);
//       }
//     });
//     client.close();
//   });
// })
//to get curated 
.get('/:page/curated',function(req,res){
  var page = req.params.page;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(page).aggregate([
      {$match:{"title":"curated"}},
      { $project : {content:1,_id:0} }]).toArray(function(err,result){
       res.send(result[0].content); 
    })
    client.close();
  })
})
//to get featured and carousel buckets
.get('/:page/:title',function(req,res){
  var page = req.params.page;
  var title = req.params.title;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(page).aggregate([
      {$match:{"title":title}},
      {$unwind:{path:"$content"}},
      { 
        $lookup:{
          from: "articles",
          localField: "content",
          foreignField: "id",
          as: "result"
        },
      },
      { $project : { _id:0,result:{id:1,headline:1,key:1,sport:1,coverImg:1} } }
    ]).toArray(function(err,result){
      var resultArr = [];
      result.forEach(el=>{
        resultArr.push(el.result[0]);
      })
      res.send(resultArr);
    })
    client.close();
  })
})
//to get videos and tweets
.get('/:page/videos_tweets', function (req, res) {
  var page = req.params.page;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(page).find({ $or: [{ type: 'video' }, { type: 'tweets' }] }).toArray(function (err, result) {
      if (err) res.send('error');else {
        res.send(result);
      }
    });
    client.close();
  });
})
//to fetch freelist
.post('/:page/fetch_freelist', function (req, res) {
  // var content = req.body.content;
  var page = req.params.page;
  var till = req.body.cursor;
  // var resultArr = {};
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    // for (var i = from; i > from + 5; i++) {
    //   el = content[i];
    //   db.collection('articles').find({ id: el }).toArray(function (err, result) {
    //     resultArr.push(result[0]);
    //   });
    // }
    db.collection(page).aggregate([
      {$match:{"title":'aotd'}},
      {$unwind:{path:"$content"}},
      {
        $lookup:{
          from:'articles',
          localField:'content',
          foreignField: 'id',
          as: 'result' 
        },
      },
      { $project : { _id:0,size:1,result:{id:1,headline:1,key:1,sport:1,coverImg:1} } },
    ]).toArray(function(err,result){
      var resultData = {};
      var resultArr = [];
      result.forEach((el,index)=>{
        if(till>=el.size){
          till = el.size-1;
          // console.log(till)
        }
        if(index<till){
          resultArr.push(el.result[0]);
        }
        else if(index==till){
          resultArr.push(el.result[0]);
          resultData.content = resultArr;
          resultData.size = el.size;
          res.send(resultData);
        }
      })
      // res.send(result);    
    })
    client.close();
  });
})
// to add a new bucket
.post('/:page/add_new_bucket', function (req, res) {
  var page = req.params.page;
  var title = req.body.title;
  var type = req.body.type;
  var limit = req.body.limit;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(page).insertOne({ title: title, type: type, content: [], limit: limit, size: 0 }, function (err, result) {
      if (err) res.send('error');else {
        res.send('succcessfull');
      }
    });
    client.close();
  });
})
//to fetch bucket names
.get('/:page/names/bucket_names', function (req, res) {
  var page = req.params.page;
  var pageBuckets = [];
  var obj = {};
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(page).find({ $or: [{ type: 'single' }, { type: 'carousel' }, { type: 'freelist' }] }, function (err, result) {
      if (err) res.send('error');else {
        result.forEach(function (el) {
          pageBuckets.push(el);
        }).then(function () {
          res.send(pageBuckets);
        });
      }
    });
    client.close();
  });
})

// to update a bucket with an article
.post('/update_bucket', function (req, res) {
  var id = req.body.id;
  var page = req.body.sport;
  var newBucket = req.body.currentBucket;
  var content = [];
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(page).find({ title: newBucket }).toArray(function (err, result) {
      console.log(result[0].size + " " + result[0].limit);
      if (err) throw res.send('1st error');else if (result[0].size == result[0].limit && result[0].type != 'freelist') {
        res.send(page + "-" + newBucket + ' is Full');
      }
    });
    db.collection(page).update({ content: id }, { $pull: { content: id }, $inc: { size: -1 } }, function (err, result) {
      if (err) res.send('3rd error');
    });
    db.collection(page).update({ title: newBucket }, { $push: { content: id }, $inc: { size: 1 } }, function (err, result) {
      if (err) res.send('4th error');else {
        res.send('successfull');
      }
    });
    client.close();
  });
})
// to change the name of bucket
.put('/:page/change_name', function (req, res) {
  var page = req.params.page;
  var title = req.body.title;
  var type = req.body.type;
  MongoClient.connect(uri, function (err, client) {
    if (err) res.send('error:' + err);
    var db = client.db("cms");
    db.collection(page).update({ type: type }, { $set: { title: title } }, function (err, result) {
      if (err) throw err;else {
        res.send('successfull');
      }
    });
    client.close();
  });
})

//to change bucket order
.post('/change_bucket_order/:bucket/:page',function (req,res){
  var content = req.body.content;
  var page = req.params.page;
  var bucket = req.params.bucket;
  MongoClient.connect(uri, function(err,client){
    if(err) res.send('error:' +err);
    var db = client.db("cms");
    db.collection(page).updateOne({title:bucket},{$set:{content:content}},function (err, result) {
      if (err) throw err;else {
        res.send('successfull');
      }
    });
    client.close();
  })
})
exports.default = bucketRouter;